# ps3linux - an independent repository for PS3 Linux & Cell development

---

![Alt text](https://aethyx.eu/wp-content/uploads/linuxonps3.jpg?raw=true "PS3 Linux FTW!")

---

This is the repository for pre-compiled PS3 Linux kernels for Debian 8.x (Jessie).

There are two directories:

* functional: these kernels should boot with OtherOS/OtherOS++. Included is the official kernel from "Red Ribbon Linux", as well as working kernels compiled by us. Use at your own risk.
* non-functional: these kernels were compiled without patches from the official ps3-linux git (vanilla) and won't run on PS3 with OtherOS++. It could be those run on OFW <=3.15 but we couldn't test it. Use at your own risk and report back, please. Also, if you got patches (or updated patches) so that these kernels would run on OtherOS++, please get back to us, see contact data below.

---

## Abstract

Browse the repository and watch carefully what you're looking for. Sometimes there exist README.md files, read them thoroughly.

Unfortunately patches for the official PS3 kernel tree seem to stop with version 3.16. There were no updates since then.

There exists a PS3 kernel tree still today: [https://git.kernel.org/pub/scm/linux/kernel/git/geoff/ps3-linux.git](https://git.kernel.org/pub/scm/linux/kernel/git/geoff/ps3-linux.git "https://git.kernel.org/pub/scm/linux/kernel/git/geoff/ps3-linux.git") maintained by Geoff Levand.
Be aware that due to a ABI bug the most recent kernel is 3.15 you may use on your own PS3 Linux machine (status: December 2017).

Feel free to contribute!

---

## Motivation

We at AETHYX MEDIAE are still trying to program internally for the Cell Broadband Engine. It's a side project.
Thus, from time to time we'll bring back stuff online we used in the past. Also docs and developer files are planned.
Thirdly, plans for publishing our stuff in the near future are under (heavy) construction.

---

## Contact

Feel free to drop us a line anytime: info [at] aethyx [dot] eu.

Thanks and have a good time!

---

## Donations

Setting up and creating this repository was a hell of work and consumed a lot of time and nerves, so donations are warmly welcome.

Please use the following addresses to send us donations:

BTC: 1Df8eapdFuxPxteZLfXnwvyVuo574psxKM

ETH: 0x0D2Ab63dfe70a7fA12f9d66eCfEA9dDc8F5173A8

XEM: NBZPMU-XES6ST-ITEBR3-IHAPTR-APGI3Y-RAAMHV-VZFJ
