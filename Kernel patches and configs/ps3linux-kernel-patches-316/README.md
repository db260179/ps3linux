# ps3linux - an independent repository for PS3 Linux & Cell development

---

![Alt text](https://aethyx.eu/wp-content/uploads/linuxonps3.jpg?raw=true "PS3 Linux FTW!")

---

ATTENTION: don't ever try to compile a kernel 3.16.x (with or without these patches), they won't boot!
The aforementioned kernel ABI breakage bug is too heavy!
We compiled kernel 3.16.56 as well as 3.16.7; our machines didn't boot _at all_!
Don't waste your time and nerves doing this if others tried already and weren't successful!
The last kernel for our PS3 was 3.15.x from 2014...

What we can do is try to build a kernel from 4.17-rc1 onwards, as support continues since August 2018(!) for FAT and DECR-1400 models, see announcement here: https://mirrors.edge.kernel.org/pub/linux/kernel/people/geoff/cell/README

We in fact tried to compile kernel 4.18, with and without patches, and our machines did boot. :-) Unfortunately it wasn't possible for unpatched kernels to find the needed ps3ddx partition. :-(
We then patched these kernels but a lot of essential patches didn't work anymore, which was expected of course. 
See our ps3linux-kernel-patches-418 folder for a HunkFailsLog as well as the according .rej files.

Let's work together now to get kernel 4.1x work again on our PS3s, it's doable! :-)

---

## Abstract

Browse the repository and watch carefully what you're looking for. Sometimes there exist README.md files, read them thoroughly.

Unfortunately patches for the official kernel tree seem to stop with version 3.16. But these shouldn't be needed anymore.

There exists a PS3 kernel tree still today: [https://git.kernel.org/pub/scm/linux/kernel/git/geoff/ps3-linux.git](https://git.kernel.org/pub/scm/linux/kernel/git/geoff/ps3-linux.git "https://git.kernel.org/pub/scm/linux/kernel/git/geoff/ps3-linux.git") maintained by Geoff Levand.
Be aware that due to a ABI bug the most recent kernel is 3.15 you may use on your own PS3 Linux machine (status: December 2017).

Feel free to contribute!

---

## Motivation

We at AETHYX MEDIAE are still trying to program internally for the Cell Broadband Engine. It's a side project.
Thus, from time to time we'll bring back stuff online we used in the past. Also docs and developer files are planned.
Thirdly, plans for publishing our stuff in the near future are under (heavy) construction.

---

## Contact

Feel free to drop us a line anytime: info [at] aethyx [dot] eu.

Thanks and have a good time!

---

## Donations

Setting up and creating this repository was a hell of work and consumed a lot of time and nerves, so donations are warmly welcome.

Please use the following addresses to send us donations:

BTC: 1Df8eapdFuxPxteZLfXnwvyVuo574psxKM

ETH: 0x0D2Ab63dfe70a7fA12f9d66eCfEA9dDc8F5173A8

XEM: NBZPMU-XES6ST-ITEBR3-IHAPTR-APGI3Y-RAAMHV-VZFJ
