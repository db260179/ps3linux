Index: linux/arch/powerpc/platforms/ps3pf/setup.c
===================================================================
--- linux.orig/arch/powerpc/platforms/ps3pf/setup.c
+++ linux/arch/powerpc/platforms/ps3pf/setup.c
@@ -432,6 +432,11 @@ extern void ps3pf_pci_shutdown(void);
 #else
 static inline void ps3pf_pci_shutdown(void) {}
 #endif
+#ifdef CONFIG_FB_PS3
+extern void ps3fb_cleanup(void);
+#else
+extern void ps3fb_cleanup(void) {}
+#endif
 
 static void ps3pf_prepare_shutdown(void)
 {
@@ -442,6 +447,7 @@ static void ps3pf_prepare_shutdown(void)
 	local_irq_disable();
 
 	ps3pf_pci_shutdown();
+	ps3fb_cleanup();
 }
 
 static void ps3pf_restart(char *cmd)
Index: linux/drivers/video/ps3fb.c
===================================================================
--- /dev/null
+++ linux/drivers/video/ps3fb.c
@@ -0,0 +1,1324 @@
+/*
+ * linux/drivers/video/ps3fb.c -- PS3PF GPU frame buffer device
+ *
+ * Copyright (C) 2006 Sony Computer Entertainment Inc.
+ * 
+ *  this file is based on :
+ *
+ *  linux/drivers/video/vfb.c -- Virtual frame buffer device
+ *
+ *      Copyright (C) 2002 James Simmons
+ *
+ *	Copyright (C) 1997 Geert Uytterhoeven
+ *
+ *  This file is subject to the terms and conditions of the GNU General Public
+ *  License. See the file COPYING in the main directory of this archive for
+ *  more details.
+ */
+
+#include <linux/module.h>
+#include <linux/kernel.h>
+#include <linux/errno.h>
+#include <linux/string.h>
+#include <linux/mm.h>
+#include <linux/tty.h>
+#include <linux/slab.h>
+#include <linux/vmalloc.h>
+#include <linux/delay.h>
+#include <linux/interrupt.h>
+#include <linux/platform_device.h>
+#include <linux/console.h>
+#include <linux/ioctl.h>
+#include <linux/notifier.h>
+#include <linux/reboot.h>
+
+#include <asm/uaccess.h>
+#include <linux/fb.h>
+#include <linux/init.h>
+#include <asm/time.h>
+
+#include <asm/abs_addr.h>
+#include <asm/lv1call.h>
+#include <asm/ps3av.h>
+#include <asm/ps3fb.h>
+
+//#define PS3FB_DEBUG
+
+#ifdef PS3FB_DEBUG
+#define DPRINTK(fmt, args...) printk("%s: " fmt, __FUNCTION__ , ##args)
+#else
+#define DPRINTK(fmt, args...)
+#endif
+
+extern unsigned long p_to_lp(long pa);
+#define PS3FB_NUM_FRAMES_MAX                        2 /* num of buffers */
+#define PS3FB_DEV                                   0 /* /dev/fb0 */
+#define L1GPU_CONTEXT_ATTRIBUTE_DISPLAY_MODE_SET    0x100
+#define L1GPU_CONTEXT_ATTRIBUTE_DISPLAY_SYNC        0x101
+#define L1GPU_CONTEXT_ATTRIBUTE_DISPLAY_FLIP        0x102
+#define L1GPU_CONTEXT_ATTRIBUTE_FB_SETUP            0x600
+#define L1GPU_CONTEXT_ATTRIBUTE_FB_BLIT             0x601
+#define L1GPU_CONTEXT_ATTRIBUTE_FB_BLIT_SYNC        0x602
+
+#define L1GPU_DISPLAY_SYNC_HSYNC                    1
+#define L1GPU_DISPLAY_SYNC_VSYNC                    2
+
+
+#define DDR_SIZE                    (0) /* used no ddr */
+#define GPU_OFFSET                  (64 * 1024)
+#define GPU_1M                      (0x100000UL)
+#define GPU_IOIF                    (0x0d000000UL)
+
+#define PS3FB_FULL_MODE_BIT         0x80
+
+#define GPU_INTR_STATUS_VSYNC_0     0 /* vsync on head A */
+#define GPU_INTR_STATUS_VSYNC_1     1 /* vsync on head B */
+#define GPU_INTR_STATUS_FLIP_0      3 /* flip head A */
+#define GPU_INTR_STATUS_FLIP_1      4 /* flip head B */
+#define GPU_INTR_STATUS_QUEUE_0     5 /* queue head A */
+#define GPU_INTR_STATUS_QUEUE_1     6 /* queue head B */
+
+#define GPU_DRIVER_INFO_VERSION     0x211
+
+/* gpu internals */
+struct display_head {
+	uint64_t	be_time_stamp;
+	uint32_t	status;
+	uint32_t	offset;
+	uint32_t	res1;
+	uint32_t	res2;
+	uint32_t	field;
+	uint32_t	reserved1;
+
+	uint64_t	res3;
+	uint32_t	raster;
+
+	uint64_t	vblank_count;
+	uint32_t	field_vsync;
+	uint32_t	reserved2;
+};
+struct gpu_irq {
+	uint32_t	irq_outlet;
+	uint32_t	status;
+	uint32_t	mask;
+	uint32_t	video_cause;
+	uint32_t	graph_cause;
+	uint32_t	user_cause;
+
+	uint32_t	res1;
+	uint64_t	res2;
+
+	uint32_t	reserved[4];
+};
+struct gpu_driver_info {
+	uint32_t	version_driver;
+	uint32_t	version_gpu;
+	uint32_t	memory_size;
+	uint32_t	hardware_channel;
+
+	uint32_t	nvcore_frequency;
+	uint32_t	memory_frequency;
+
+	uint32_t	reserved[1063];
+	struct		display_head display_head[8];
+	struct		gpu_irq irq;
+};
+
+
+struct ps3fb_priv {
+	u_long		videomemory_phys;
+	u_long		videomemorysize;
+	uint32_t	irq_outlet;
+	unsigned int	irq_no, irq_cpu;
+	void		*dev;
+
+	uint64_t	context_handle, memory_handle;
+	uint64_t	ddr_ea, xdr_ea, dinfo_ea;
+	struct		semaphore sem;
+	uint32_t	res_index;
+
+	uint64_t	be_time_stamp;
+	uint64_t	vblank_count; /* frame count */
+	uint32_t	field_vsync; /* even/odd field */
+	wait_queue_head_t wait_vsync;
+
+	uint32_t	num_frames; /* num of frame buffers */
+	atomic_t	ext_flip; /* on/off flip with vsync */
+	atomic_t	f_count; /* fb_open count */
+};
+static struct ps3fb_priv ps3fb;
+
+
+struct ps3fb_res_table {
+	uint32_t xres;
+	uint32_t yres;
+	uint32_t xoff;
+	uint32_t yoff;
+	uint32_t type;
+};
+#define PS3FB_RES_FULL 1
+const struct ps3fb_res_table ps3fb_res[] = {
+	/* res_x,y   margin_x,y  full */
+	{  720,  480,  72,  48 , 0},
+	{  720,  576,  72,  58 , 0},
+	{ 1280,  720,  78,  38 , 0},
+	{ 1920, 1080, 116,  58 , 0},
+	/* full mode */
+	{  720,  480,   0,   0 , PS3FB_RES_FULL},
+	{  720,  576,   0,   0 , PS3FB_RES_FULL},
+	{ 1280,  720,   0,   0 , PS3FB_RES_FULL},
+	{ 1920, 1080,   0,   0 , PS3FB_RES_FULL},
+	/* vesa: normally full mode */
+	{ 1280,  768,   0,   0 , 0},
+	{ 1280, 1024,   0,   0 , 0},
+	{ 1920, 1200,   0,   0 , 0},
+	{    0,    0,   0,   0 , 0} };
+
+/* default resolution */
+#define GPU_RES_INDEX 0 /* 720 x 480 */
+
+
+    /*
+     *  RAM we reserve for the frame buffer. This defines the maximum screen
+     *  size
+     *
+     *  The default can be overridden if the driver is compiled as a module
+     */
+
+#define HEAD_A
+#define HEAD_B
+
+#define VIDEOMEMSIZE	((9*1024*1024) * PS3FB_NUM_FRAMES_MAX)
+
+#define X_OFF(i)	(ps3fb_res[i].xoff)	/* left/right margin (pixel) */
+#define Y_OFF(i)	(ps3fb_res[i].yoff)	/* top/bottom margin (pixel) */
+#define WIDTH(i)	(ps3fb_res[i].xres)	/* width of FB */
+#define HEIGHT(i)	(ps3fb_res[i].yres)	/* height of FB */
+#define BPP	4	/* number of bytes per pixel */
+#define VP_OFF(i)	(WIDTH(i) * Y_OFF(i) * BPP + X_OFF(i) * BPP)
+#define FB_OFF(i)	(GPU_OFFSET - VP_OFF(i) % GPU_OFFSET)
+
+char videomemory[VIDEOMEMSIZE + GPU_1M] __attribute__((aligned(32 * 1024)));
+
+static int ps3fb_mode = 0;
+module_param(ps3fb_mode, bool, 0);
+
+static int get_res_table(uint32_t xres, uint32_t yres);
+static int get_pixclock(int index);
+static int ps3fb_check_var(struct fb_var_screeninfo *var,
+			 struct fb_info *info);
+static int ps3fb_set_par(struct fb_info *info);
+static int ps3fb_setcolreg(u_int regno, u_int red, u_int green, u_int blue,
+			 u_int transp, struct fb_info *info);
+static int ps3fb_pan_display(struct fb_var_screeninfo *var,
+			   struct fb_info *info);
+static int ps3fb_mmap(struct fb_info *info,
+		    struct vm_area_struct *vma);
+static int ps3fb_blank(int blank, struct fb_info *info);
+static int ps3fb_ioctl(struct fb_info *info, unsigned int cmd, 
+			unsigned long arg);
+static int ps3fb_sync(uint32_t);
+
+static int ps3fb_open(struct fb_info *info, int user)
+{
+	atomic_inc(&ps3fb.f_count);
+	return 0;
+}
+static int ps3fb_release(struct fb_info *info, int user)
+{
+	if (atomic_dec_and_test(&ps3fb.f_count)) {
+		if (atomic_read(&ps3fb.ext_flip)) {
+			atomic_set(&ps3fb.ext_flip, 0);
+			ps3fb_sync(0); /* single buffer */
+		}
+	}
+	return 0;
+}
+static struct fb_ops ps3fb_ops = {
+	.fb_open	= ps3fb_open,
+	.fb_release	= ps3fb_release,
+	.fb_check_var	= ps3fb_check_var,
+	.fb_set_par	= ps3fb_set_par,
+	.fb_setcolreg	= ps3fb_setcolreg,
+	.fb_pan_display	= ps3fb_pan_display,
+	.fb_fillrect	= cfb_fillrect,
+	.fb_copyarea	= cfb_copyarea,
+	.fb_imageblit	= cfb_imageblit,
+	.fb_mmap	= ps3fb_mmap,
+	.fb_blank	= ps3fb_blank,
+	.fb_ioctl	= ps3fb_ioctl,
+	.fb_compat_ioctl = ps3fb_ioctl
+};
+
+static struct fb_var_screeninfo ps3fb_default;
+
+void ps3fb_fb_var_default(void)
+{
+	uint32_t res_index = ps3fb.res_index;
+	ps3fb_default.xres = WIDTH(res_index) -  2 * X_OFF(res_index);
+	ps3fb_default.yres = HEIGHT(res_index) - 2 * Y_OFF(res_index);
+	ps3fb_default.xres_virtual = 
+				WIDTH(res_index) -  2 * X_OFF(res_index);
+	ps3fb_default.yres_virtual = 
+				HEIGHT(res_index) - 2 * Y_OFF(res_index);
+	ps3fb_default.xoffset = 0;
+	ps3fb_default.yoffset = 0;
+	ps3fb_default.bits_per_pixel = 32;
+	ps3fb_default.red.offset = 16;
+	ps3fb_default.red.length = 8;
+	ps3fb_default.red.msb_right = 0;
+	ps3fb_default.green.offset = 8;
+	ps3fb_default.green.length = 8;
+	ps3fb_default.green.msb_right = 0;
+	ps3fb_default.blue.offset = 0;
+	ps3fb_default.blue.length = 8;
+	ps3fb_default.blue.msb_right = 0;
+	ps3fb_default.transp.offset = 24;
+	ps3fb_default.transp.length =8;
+	ps3fb_default.transp.msb_right =0;
+	ps3fb_default.activate = FB_ACTIVATE_NOW;
+	ps3fb_default.height = -1;
+	ps3fb_default.width = -1;
+	ps3fb_default.pixclock = get_pixclock(res_index);
+	ps3fb_default.left_margin = 0;
+	ps3fb_default.right_margin = 0;
+	ps3fb_default.upper_margin = 0;
+	ps3fb_default.lower_margin = 0;
+	ps3fb_default.hsync_len = 0;
+	ps3fb_default.vsync_len = 0;
+	ps3fb_default.vmode =	FB_VMODE_NONINTERLACED;
+}
+
+static struct fb_fix_screeninfo ps3fb_fix __initdata = {
+	.id =		"PS3 FB",
+	.type =		FB_TYPE_PACKED_PIXELS,
+	.visual =	FB_VISUAL_TRUECOLOR,
+	.xpanstep =	1,
+	.ypanstep =	1,
+	.ywrapstep =	1,
+	.accel =	FB_ACCEL_NONE,
+};
+
+    /*
+     *  Internal routines
+     */
+static int get_res_table(uint32_t xres, uint32_t yres)
+{
+	int i, full_mode;
+	uint32_t x, y, f = 0;
+
+	full_mode = (ps3fb_mode & PS3FB_FULL_MODE_BIT) ? PS3FB_RES_FULL : 0;
+	for (i = 0; ;i++) {
+		x = ps3fb_res[i].xres;
+		y = ps3fb_res[i].yres;
+		f = ps3fb_res[i].type;
+		if (full_mode == PS3FB_RES_FULL && f != PS3FB_RES_FULL) {
+			continue;
+		}
+		if (x == 0) {
+			DPRINTK("ERROR: get_res_table()\n");
+			return -1;
+		}
+		if(x == xres && (yres == 0 || y == yres)) break;
+		x = x - 2 * ps3fb_res[i].xoff;
+		y = y - 2 * ps3fb_res[i].yoff;
+		if (x == xres && (yres == 0 || y == yres)) break; 
+	}
+	return i;
+}
+
+static u_long get_line_length(int xres_virtual, int bpp)
+{
+	u_long length;
+	int i;
+
+	i = get_res_table(xres_virtual, 0);
+	if (i == -1) return ps3fb_res[GPU_RES_INDEX].xres * 4;
+
+	xres_virtual = ps3fb_res[i].xres;
+
+	length = xres_virtual * bpp;
+	length = (length + 31) & ~31;
+	length >>= 3;
+	DPRINTK("xres_virtual:%d length byte:%ld\n", xres_virtual, length);
+	return (length);
+}
+
+static int get_pixclock(int index)
+{
+	uint32_t x, y, rate, pixclock;
+	x = ps3fb_res[index].xres - 2 * ps3fb_res[index].xoff;
+	y = ps3fb_res[index].yres - 2 * ps3fb_res[index].yoff;
+
+	rate = ps3av_get_refresh_rate(ps3av_get_mode());
+	pixclock = 1000000000000L / rate / x / y;
+	return pixclock;
+}
+
+    /*
+     *  Setting the video mode has been split into two parts.
+     *  First part, xxxfb_check_var, must not write anything
+     *  to hardware, it should only verify and adjust var.
+     *  This means it doesn't alter par but it does use hardware
+     *  data from it to check this var. 
+     */
+
+static int ps3fb_check_var(struct fb_var_screeninfo *var,
+			 struct fb_info *info)
+{
+	u_long line_length;
+	int i;
+
+	DPRINTK("var->xres:%d info->var.xres:%d\n", var->xres, info->var.xres); 
+	DPRINTK("var->yres:%d info->var.yres:%d\n", var->yres, info->var.yres);
+
+	i = get_res_table(var->xres, var->yres);
+	if (i == -1) {
+		DPRINTK("invalid resolution \n"); 
+		return -EINVAL;
+	}
+
+	/*
+	 *  FB_VMODE_CONUPDATE and FB_VMODE_SMOOTH_XPAN are equal!
+	 *  as FB_VMODE_SMOOTH_XPAN is only used internally
+	 */
+
+	if (var->vmode & FB_VMODE_CONUPDATE) {
+		var->vmode |= FB_VMODE_YWRAP;
+		var->xoffset = info->var.xoffset;
+		var->yoffset = info->var.yoffset;
+	}
+
+	/*
+	 *  Some very basic checks
+	 */
+	if (!var->xres)
+		var->xres = 1;
+	if (!var->yres)
+		var->yres = 1;
+	if (var->xres > var->xres_virtual)
+		var->xres_virtual = var->xres;
+	if (var->yres > var->yres_virtual)
+		var->yres_virtual = var->yres;
+	if (var->bits_per_pixel <= 1)
+		var->bits_per_pixel = 1;
+	else if (var->bits_per_pixel <= 8)
+		var->bits_per_pixel = 8;
+	else if (var->bits_per_pixel <= 16)
+		var->bits_per_pixel = 16;
+	else if (var->bits_per_pixel <= 24)
+		var->bits_per_pixel = 24;
+	else if (var->bits_per_pixel <= 32)
+		var->bits_per_pixel = 32;
+	else
+		return -EINVAL;
+
+	if (var->xres_virtual < var->xoffset + var->xres)
+		var->xres_virtual = var->xoffset + var->xres;
+	if (var->yres_virtual < var->yoffset + var->yres)
+		var->yres_virtual = var->yoffset + var->yres;
+
+	/*
+	 *  Memory limit
+	 */
+	line_length =
+	    get_line_length(var->xres_virtual, var->bits_per_pixel);
+	if (line_length * var->yres_virtual > ps3fb.videomemorysize)
+		return -ENOMEM;
+
+	/*
+	 * Now that we checked it we alter var. The reason being is that the video
+	 * mode passed in might not work but slight changes to it might make it 
+	 * work. This way we let the user know what is acceptable.
+	 */
+	switch (var->bits_per_pixel) {
+	case 1:
+	case 8:
+		var->red.offset = 0;
+		var->red.length = 8;
+		var->green.offset = 0;
+		var->green.length = 8;
+		var->blue.offset = 0;
+		var->blue.length = 8;
+		var->transp.offset = 0;
+		var->transp.length = 0;
+		break;
+	case 16:		/* RGBA 5551 */
+		if (var->transp.length) {
+			var->red.offset = 0;
+			var->red.length = 5;
+			var->green.offset = 5;
+			var->green.length = 5;
+			var->blue.offset = 10;
+			var->blue.length = 5;
+			var->transp.offset = 15;
+			var->transp.length = 1;
+		} else {	/* RGB 565 */
+			var->red.offset = 0;
+			var->red.length = 5;
+			var->green.offset = 5;
+			var->green.length = 6;
+			var->blue.offset = 11;
+			var->blue.length = 5;
+			var->transp.offset = 0;
+			var->transp.length = 0;
+		}
+		break;
+	case 24:		/* RGB 888 */
+		var->red.offset = 16;
+		var->red.length = 8;
+		var->green.offset = 8;
+		var->green.length = 8;
+		var->blue.offset = 0;
+		var->blue.length = 8;
+		var->transp.offset = 0;
+		var->transp.length = 0;
+		break;
+	case 32:		/* RGBA 8888 */
+		var->red.offset = 16;
+		var->red.length = 8;
+		var->green.offset = 8;
+		var->green.length = 8;
+		var->blue.offset = 0;
+		var->blue.length = 8;
+		var->transp.offset = 24;
+		var->transp.length = 8;
+		break;
+	}
+	var->red.msb_right = 0;
+	var->green.msb_right = 0;
+	var->blue.msb_right = 0;
+	var->transp.msb_right = 0;
+
+	return 0;
+}
+
+/* This routine actually sets the video mode. It's in here where we
+ * the hardware state info->par and fix which can be affected by the 
+ * change in par. For this driver it doesn't do much. 
+ */
+static int ps3fb_set_par(struct fb_info *info)
+{
+	int i;
+	DPRINTK("xres:%d xv:%d yres:%d yv:%d clock:%d\n", 
+		info->var.xres, info->var.xres_virtual, 
+		info->var.yres, info->var.yres_virtual, info->var.pixclock);
+	/* XXX info->fix.line_length should originally be fixed value */
+	info->fix.line_length = get_line_length(info->var.xres_virtual,
+						info->var.bits_per_pixel);
+	i = get_res_table(info->var.xres, info->var.yres);
+	ps3fb.res_index = i;
+	info->screen_base = (char __iomem *)ps3fb.xdr_ea + FB_OFF(i) + VP_OFF(i);
+	memset((void *)ps3fb.xdr_ea, 0, ps3fb.videomemorysize);
+	return 0;
+}
+
+    /*
+     *  Set a single color register. The values supplied are already
+     *  rounded down to the hardware's capabilities (according to the
+     *  entries in the var structure). Return != 0 for invalid regno.
+     */
+
+static int ps3fb_setcolreg(u_int regno, u_int red, u_int green, u_int blue,
+			 u_int transp, struct fb_info *info)
+{
+	if (regno >= 256)	/* no. of hw registers */
+		return 1;
+	/*
+	 * Program hardware... do anything you want with transp
+	 */
+
+	/* grayscale works only partially under directcolor */
+	if (info->var.grayscale) {
+		/* grayscale = 0.30*R + 0.59*G + 0.11*B */
+		red = green = blue =
+		    (red * 77 + green * 151 + blue * 28) >> 8;
+	}
+
+	/* Directcolor:
+	 *   var->{color}.offset contains start of bitfield
+	 *   var->{color}.length contains length of bitfield
+	 *   {hardwarespecific} contains width of RAMDAC
+	 *   cmap[X] is programmed to (X << red.offset) | (X << green.offset) | (X << blue.offset)
+	 *   RAMDAC[X] is programmed to (red, green, blue)
+	 * 
+	 * Pseudocolor:
+	 *    uses offset = 0 && length = RAMDAC register width.
+	 *    var->{color}.offset is 0
+	 *    var->{color}.length contains widht of DAC
+	 *    cmap is not used
+	 *    RAMDAC[X] is programmed to (red, green, blue)
+	 * Truecolor:
+	 *    does not use DAC. Usually 3 are present.
+	 *    var->{color}.offset contains start of bitfield
+	 *    var->{color}.length contains length of bitfield
+	 *    cmap is programmed to (red << red.offset) | (green << green.offset) |
+	 *                      (blue << blue.offset) | (transp << transp.offset)
+	 *    RAMDAC does not exist
+	 */
+#define CNVT_TOHW(val,width) ((((val)<<(width))+0x7FFF-(val))>>16)
+	switch (info->fix.visual) {
+	case FB_VISUAL_TRUECOLOR:
+	case FB_VISUAL_PSEUDOCOLOR:
+		red = CNVT_TOHW(red, info->var.red.length);
+		green = CNVT_TOHW(green, info->var.green.length);
+		blue = CNVT_TOHW(blue, info->var.blue.length);
+		transp = CNVT_TOHW(transp, info->var.transp.length);
+		break;
+	case FB_VISUAL_DIRECTCOLOR:
+		red = CNVT_TOHW(red, 8);	/* expect 8 bit DAC */
+		green = CNVT_TOHW(green, 8);
+		blue = CNVT_TOHW(blue, 8);
+		/* hey, there is bug in transp handling... */
+		transp = CNVT_TOHW(transp, 8);
+		break;
+	}
+#undef CNVT_TOHW
+	/* Truecolor has hardware independent palette */
+	if (info->fix.visual == FB_VISUAL_TRUECOLOR) {
+		u32 v;
+
+		if (regno >= 16)
+			return 1;
+
+		v = (red << info->var.red.offset) |
+		    (green << info->var.green.offset) |
+		    (blue << info->var.blue.offset) |
+		    (transp << info->var.transp.offset);
+		switch (info->var.bits_per_pixel) {
+		case 8:
+			break;
+		case 16:
+			((u32 *) (info->pseudo_palette))[regno] = v;
+			break;
+		case 24:
+		case 32:
+			((u32 *) (info->pseudo_palette))[regno] = v;
+			break;
+		}
+		return 0;
+	}
+	return 0;
+}
+
+    /*
+     *  Pan or Wrap the Display
+     *
+     *  This call looks only at xoffset, yoffset and the FB_VMODE_YWRAP flag
+     */
+
+static int ps3fb_pan_display(struct fb_var_screeninfo *var,
+			   struct fb_info *info)
+{
+	if (var->vmode & FB_VMODE_YWRAP) {
+		if (var->yoffset < 0
+		    || var->yoffset >= info->var.yres_virtual
+		    || var->xoffset)
+			return -EINVAL;
+	} else {
+		if (var->xoffset + var->xres > info->var.xres_virtual ||
+		    var->yoffset + var->yres > info->var.yres_virtual)
+			return -EINVAL;
+	}
+	info->var.xoffset = var->xoffset;
+	info->var.yoffset = var->yoffset;
+	if (var->vmode & FB_VMODE_YWRAP)
+		info->var.vmode |= FB_VMODE_YWRAP;
+	else
+		info->var.vmode &= ~FB_VMODE_YWRAP;
+	return 0;
+}
+
+    /*
+     *  Most drivers don't need their own mmap function 
+     */
+
+static int ps3fb_mmap(struct fb_info *info,
+		    struct vm_area_struct *vma)
+{
+#ifdef CONFIG_PPC_PS3PF
+	unsigned long size = vma->vm_end - vma->vm_start;
+	unsigned long offset = vma->vm_pgoff << PAGE_SHIFT;
+	int i;
+
+	i = get_res_table(info->var.xres, info->var.yres);
+	if (i == -1) return -EINVAL;
+	
+	if (offset + size > ps3fb.videomemorysize)
+		return -EINVAL;
+	offset += ps3fb.videomemory_phys + FB_OFF(i) + VP_OFF(i);
+	if (remap_pfn_range(vma, vma->vm_start, offset >> PAGE_SHIFT,
+			    size, vma->vm_page_prot))
+		return -EAGAIN;
+	printk(KERN_DEBUG "ps3fb: mmap framebuffer P(%lx)->V(%lx)\n",
+	       offset, vma->vm_start);
+	return 0;
+#else
+	return -EINVAL;
+#endif
+}
+    
+    /*
+     * Blank the display 
+     */
+
+static int ps3fb_blank(int blank, struct fb_info *info)
+{
+	int retval = 0;
+
+	DPRINTK("%s: blank:%d\n", __FUNCTION__, blank);
+	switch (blank) {
+	case FB_BLANK_POWERDOWN:
+	case FB_BLANK_HSYNC_SUSPEND:
+	case FB_BLANK_VSYNC_SUSPEND:
+	case FB_BLANK_NORMAL:
+		retval = ps3av_video_mute(1); /* mute on */
+		break;
+	default:	/* unblank */
+		retval = ps3av_video_mute(0); /* mute off */
+		break;
+	}
+	return retval;
+}
+
+static int ps3fb_get_vblank(struct fb_vblank *vblank)
+{
+	memset(vblank, 0, sizeof(&vblank));
+	vblank->flags = FB_VBLANK_HAVE_VSYNC;
+	return 0;
+}
+
+int ps3fb_wait_for_vsync(uint32_t crtc)
+{
+	int ret;
+	uint64_t count;
+
+	count = ps3fb.vblank_count;
+	ret = wait_event_interruptible_timeout(ps3fb.wait_vsync,
+				count != ps3fb.vblank_count, HZ/10);
+	if (ret == 0) {
+		return -ETIMEDOUT;
+	}
+	return 0;
+}
+EXPORT_SYMBOL(ps3fb_wait_for_vsync);
+
+int ps3fb_flip_ctl(int on)
+{
+	if (on) {
+		if (atomic_read(&ps3fb.ext_flip) > 0) {
+			atomic_dec(&ps3fb.ext_flip);
+		}
+	} else {
+		atomic_inc(&ps3fb.ext_flip);
+	}
+	return 0;
+}
+EXPORT_SYMBOL(ps3fb_flip_ctl);
+
+    /*
+     * ioctl 
+     */
+
+static int ps3fb_ioctl(struct fb_info *info, unsigned int cmd, 
+			unsigned long arg)
+{
+	void __user *argp = (void __user *)arg;
+	uint32_t val, old_mode;
+	int retval = 0;
+
+	switch(cmd) {
+		case FBIOGET_VBLANK:
+		{
+			struct fb_vblank vblank;
+			DPRINTK("FBIOGET_VBLANK:\n");
+			retval = ps3fb_get_vblank(&vblank);
+			if (!retval) {
+				if (copy_to_user(argp, &vblank,
+					sizeof(vblank))) {
+					retval = -EFAULT;
+				}
+			}
+			break;
+		}
+		case FBIO_WAITFORVSYNC:
+		{
+			uint32_t crt;
+			DPRINTK("FBIO_WAITFORVSYNC:\n");
+			if (get_user(crt, (uint32_t __user *)arg)) {
+				retval = -EFAULT;
+				break;
+			}
+			retval = ps3fb_wait_for_vsync(crt);
+			break;
+		}
+		case PS3FB_IOCTL_SETMODE:
+			if (copy_from_user(&val, argp, sizeof(val))) {
+				retval = -EFAULT;
+				break;
+			}
+			DPRINTK("PS3FB_IOCTL_SETMODE:%x\n", val);
+			old_mode = ps3fb_mode;
+			ps3fb_mode = val;
+			if (ps3av_set_video_mode(val, 0)) {
+				ps3fb_mode = old_mode;
+				retval = -EFAULT;
+			}
+			break;
+		case PS3FB_IOCTL_GETMODE:
+			val = ps3av_get_mode();
+			DPRINTK("PS3FB_IOCTL_GETMODE:%x\n", val);
+			if (copy_to_user(argp, &val, sizeof(val))) {
+				retval = -EFAULT;
+			}
+			break;
+		case PS3FB_IOCTL_SCREENINFO:
+		{
+			struct ps3fb_ioctl_res res;
+			int i = ps3fb.res_index;
+			DPRINTK("PS3FB_IOCTL_SCREENINFO:\n");
+			res.xres = ps3fb_res[i].xres;
+			res.yres = ps3fb_res[i].yres;
+			res.xoff = ps3fb_res[i].xoff;
+			res.yoff = ps3fb_res[i].yoff;
+			res.num_frames = ps3fb.num_frames;
+			if (copy_to_user(argp, &res, sizeof(res))) {
+				retval = -EFAULT;
+			}
+			break;
+		}
+		case PS3FB_IOCTL_ON:
+			DPRINTK("PS3FB_IOCTL_ON:\n");
+			atomic_inc(&ps3fb.ext_flip);
+			break;
+		case PS3FB_IOCTL_OFF:
+			DPRINTK("PS3FB_IOCTL_OFF:\n");
+			if (atomic_read(&ps3fb.ext_flip) > 0) {
+				atomic_dec(&ps3fb.ext_flip);
+			}
+			break;
+		case PS3FB_IOCTL_FSEL:
+			if(copy_from_user(&val, argp, sizeof(val))) {
+				retval = -EFAULT;
+				break;
+			}
+			DPRINTK("PS3FB_IOCTL_FSEL:%d\n", val);
+			retval = ps3fb_sync(val);
+			break;
+		default:
+			retval = -ENOTTY;
+			break;
+	}
+	return retval;
+}
+
+static int ps3fb_sync(uint32_t val)
+{
+	int i;
+	uint32_t xres, yres;
+	uint32_t *fb;
+	uint64_t head, fb_ioif, offset;
+	uint64_t status, sync = 1;
+
+	i = ps3fb.res_index;
+	xres = ps3fb_res[i].xres;
+	yres = ps3fb_res[i].yres;
+
+	if (val > PS3FB_NUM_FRAMES_MAX -1) {
+		printk("invalid num frames\n");
+		return -EINVAL;
+	}
+	offset = xres * yres * BPP * val;
+
+	fb = (u32 *)(virt_to_abs(ps3fb.xdr_ea) + FB_OFF(i) + offset);
+	fb_ioif = GPU_IOIF + FB_OFF(i) + offset;
+	status = lv1_gpu_context_attribute(ps3fb.context_handle,
+				L1GPU_CONTEXT_ATTRIBUTE_FB_BLIT,
+				offset,
+				fb_ioif,
+				(sync <<32) | (xres<< 16) | (yres << 0), 
+				xres * BPP); /* line_length */
+	if (status) {
+		printk("lv1_gpu_context_attribute FB_BLIT failed: status %ld\n",
+			status);
+	}
+
+#ifdef HEAD_A
+	head = 0;
+	status = lv1_gpu_context_attribute(ps3fb.context_handle,
+		L1GPU_CONTEXT_ATTRIBUTE_DISPLAY_FLIP, head, offset, 0, 0);
+	if (status) {
+		printk("lv1_gpu_context_attribute FLIP failed: status %ld\n",
+				status);
+	}
+
+#endif
+#ifdef HEAD_B
+	head = 1;
+	status = lv1_gpu_context_attribute(ps3fb.context_handle,
+		L1GPU_CONTEXT_ATTRIBUTE_DISPLAY_FLIP, head, offset, 0, 0);
+	if (status) {
+		printk("lv1_gpu_context_attribute FLIP failed: status %ld\n",
+				status);
+	}
+#endif
+	return 0;
+}
+
+static int ps3fbd(void *arg)
+{
+	daemonize("ps3fbd");
+	for (;;) {
+		down(&ps3fb.sem);
+		if (atomic_read(&ps3fb.ext_flip) == 0) {
+			ps3fb_sync(0); /* single buffer */
+		}
+	}
+}
+
+static irqreturn_t ps3fb_vsync_interrupt(int irq, void *ptr, struct pt_regs *regs)
+{
+	uint32_t status;
+	uint64_t v1, retval;
+	struct gpu_driver_info *dinfo = (struct gpu_driver_info *)ps3fb.dinfo_ea;
+
+	retval = lv1_gpu_context_intr(ps3fb.context_handle, &v1);
+	if (retval) {
+		printk("lv1_gpu_context_intr failed:%ld\n", retval);
+		return IRQ_NONE;
+	}
+	status = (uint32_t)v1;
+
+	if (status & ( 1 << GPU_INTR_STATUS_VSYNC_1) ) {
+		/* VSYNC */
+		ps3fb.be_time_stamp = dinfo->display_head[1].be_time_stamp;
+		ps3fb.vblank_count = dinfo->display_head[1].vblank_count;
+		ps3fb.field_vsync = dinfo->display_head[1].field_vsync;
+		up(&ps3fb.sem);
+		wake_up_interruptible(&ps3fb.wait_vsync);
+	}
+
+	return IRQ_HANDLED;
+}
+
+extern struct fb_info *registered_fb[];
+int ps3fb_set_resolution(uint32_t x, uint32_t y)
+{
+	struct fb_var_screeninfo var;
+	struct fb_info *info = registered_fb[PS3FB_DEV];
+
+	var = info->var;
+	DPRINTK("ps3fb_set_resolution: x:y %d:%d to %d:%d\n", 
+			var.xres, var.yres, x, y);
+	var.xres = x;
+	var.yres = y;
+	var.xres_virtual = x;
+	var.yres_virtual = y;
+	var.pixclock = get_pixclock(get_res_table(x, y));
+
+	acquire_console_sem();
+	info->flags |= FBINFO_MISC_USEREVENT;
+	fb_set_var(info, &var);
+	info->flags &= ~FBINFO_MISC_USEREVENT;
+	release_console_sem();
+	return 0;
+}
+EXPORT_SYMBOL(ps3fb_set_resolution);
+
+int ps3fb_get_res(uint32_t xres, uint32_t yres, uint32_t *x, uint32_t *y)
+{
+	int i;
+	i = get_res_table(xres, yres);
+	if (i == -1) return -1;
+
+	*x = ps3fb_res[i].xres - 2 * ps3fb_res[i].xoff;
+	*y = ps3fb_res[i].yres - 2 * ps3fb_res[i].yoff;
+
+	return 0;
+}
+EXPORT_SYMBOL(ps3fb_get_res);
+
+#ifndef MODULE
+static int __init ps3fb_setup(char *options)
+{
+	char *this_opt;
+	int mode = 0;
+
+	if (!options || !*options)
+		return 0; /* no options */
+
+	while ((this_opt = strsep(&options, ",")) != NULL) {
+		if (!*this_opt)
+			continue;
+		if (!strncmp(this_opt, "mode:", 5))
+			mode = simple_strtoul(this_opt+5, NULL, 0);
+	}
+	return mode;
+}
+#endif  /*  MODULE  */
+
+    /*
+     *  Initialisation
+     */
+
+static void ps3fb_platform_release(struct device *device)
+{
+	// This is called when the reference count goes to zero.
+}
+
+static int vsync_settings(uint64_t dinfo_ea, void *dev)
+{
+	int retval;
+	struct gpu_driver_info *dinfo = (struct gpu_driver_info *)dinfo_ea;
+
+	DPRINTK("version_driver:%x\n", dinfo->version_driver);
+	DPRINTK("irq outlet:%x\n", dinfo->irq.irq_outlet);
+	DPRINTK("version_gpu:%x memory_size:%x ch:%x core_freq:%d mem_freq:%d \n",
+		dinfo->version_gpu, dinfo->memory_size, dinfo->hardware_channel,
+		dinfo->nvcore_frequency/1000000, dinfo->memory_frequency/1000000);
+
+	if (dinfo->version_driver != GPU_DRIVER_INFO_VERSION) {
+		printk("version_driver err:%x\n", dinfo->version_driver);
+		return -EINVAL;
+	}
+	ps3fb.irq_outlet = dinfo->irq.irq_outlet;
+	ps3fb.dev = (void *)dev;
+	retval = __ps3pf_connect_irq(ps3fb.irq_outlet, 
+						&ps3fb.irq_no, &ps3fb.irq_cpu,
+						ps3fb_vsync_interrupt,
+						0,
+						"ps3fb vsync",
+						ps3fb.dev);
+	if (retval) {
+		printk("__ps3pf_connect_irq faild:%d\n", retval);
+		return retval;
+	}
+	dinfo->irq.mask = (1 << GPU_INTR_STATUS_VSYNC_1)
+			  | (1 << GPU_INTR_STATUS_FLIP_1);
+	return 0;
+}
+
+static int xdr_settings(uint64_t xdr_lpar)
+{
+	uint64_t status;
+	
+	status = lv1_gpu_context_iomap(ps3fb.context_handle, GPU_IOIF, 
+			xdr_lpar, ps3fb.videomemorysize, 0);
+	if (status) {
+		printk("lv1_gpu_context_iomap failed: status :%ld\n",
+				status);
+		return -ENXIO;
+	}
+	DPRINTK("video:%p xdr_ea:%lx ioif:%lx lpar:%lx phys:%lx size:%lx\n", 
+			videomemory, ps3fb.xdr_ea, GPU_IOIF, xdr_lpar,
+			virt_to_abs(ps3fb.xdr_ea), ps3fb.videomemorysize);
+
+	status = lv1_gpu_context_attribute(ps3fb.context_handle, 
+			L1GPU_CONTEXT_ATTRIBUTE_FB_SETUP,
+			xdr_lpar, ps3fb.videomemorysize, GPU_IOIF, 0);
+	if (status) {
+		printk("lv1_gpu_context_attribute FB_SETUP failed: status :%ld\n",
+				status);
+		return -ENXIO;
+	}
+	return 0;
+}
+
+static int __init ps3fb_probe(struct platform_device *dev)
+{
+	struct fb_info *info;
+	int retval = -ENOMEM;
+	
+	uint64_t ddr_lpar = 0;
+	uint64_t lpar_dma_control =0;
+	uint64_t lpar_driver_info =0;
+	uint64_t lpar_reports =0;
+	uint64_t lpar_reports_size =0;
+	uint64_t xdr_lpar;
+	uint64_t status;
+
+	/* get gpu context handle */
+	status = lv1_gpu_memory_allocate(
+			DDR_SIZE,
+			0, 0, 0, 0,
+			&ps3fb.memory_handle, &ddr_lpar);
+	if (status) {
+		printk("lv1_gpu_memory_allocate failed: status :%ld\n", status);
+		return retval;
+	}
+	ps3fb.ddr_ea = (uint64_t)ioremap(ddr_lpar, DDR_SIZE);
+
+	DPRINTK("ddr:lpar:%lx ea:%lx\n", ddr_lpar, ps3fb.ddr_ea);
+
+	status = lv1_gpu_context_allocate(ps3fb.memory_handle, 0,
+		&ps3fb.context_handle, 
+		&lpar_dma_control, &lpar_driver_info, 
+		&lpar_reports, &lpar_reports_size);
+	if (status) {
+		printk("lv1_gpu_context_attribute failed: status :%ld\n", status);
+		goto err_gpu_memory_free;
+	}
+
+	/* vsync interrupt */
+	ps3fb.dinfo_ea = (uint64_t)ioremap(lpar_driver_info, 128 * 1024);
+	retval = vsync_settings(ps3fb.dinfo_ea, (void *)dev);
+	if (retval) {
+		goto err_gpu_context_free;
+	}
+
+	/* xdr frame buffer */
+	ps3fb.xdr_ea = ((uint64_t)videomemory + (GPU_1M - 1)) & ~(GPU_1M - 1) ;
+	xdr_lpar = p_to_lp(__pa(ps3fb.xdr_ea));
+	retval = xdr_settings(xdr_lpar);
+	if (retval) {
+		goto err_free_irq;
+	}
+
+	ps3fb.videomemory_phys = virt_to_abs(ps3fb.xdr_ea);
+
+	/*
+	 * PS3FB must clear memory to prevent kernel info
+	 * leakage into userspace
+	 * VGA-based drivers MUST NOT clear memory if
+	 * they want to be able to take over vgacon
+	 */
+	memset((void *)ps3fb.xdr_ea, 0, ps3fb.videomemorysize);
+	info = framebuffer_alloc(sizeof(u32) * 256, &dev->dev);
+	if (!info)
+		goto err_free_irq;
+
+	ps3fb_fb_var_default();
+
+	info->screen_base = (char __iomem *)ps3fb.xdr_ea + 
+			FB_OFF(ps3fb.res_index) + VP_OFF(ps3fb.res_index);
+	info->fbops = &ps3fb_ops;
+
+	info->var = ps3fb_default;
+	info->fix = ps3fb_fix;
+	info->fix.smem_len = ps3fb.videomemorysize;
+	info->pseudo_palette = info->par;
+	info->par = NULL;
+	info->flags = FBINFO_FLAG_DEFAULT;
+
+	retval = fb_alloc_cmap(&info->cmap, 256, 0);
+	if (retval < 0) {
+		goto err_framebuffer_release;
+	}
+
+	retval = register_framebuffer(info);
+	if (retval < 0) {
+		goto err_fb_dealloc;
+	}
+	platform_set_drvdata(dev, info);
+
+	printk(KERN_INFO
+	       "fb%d: GPU frame buffer device, using %ldK of video memory\n",
+	       info->node, ps3fb.videomemorysize >> 10);
+
+	kernel_thread(ps3fbd, info, CLONE_KERNEL);
+
+	ps3av_set_video_mode(ps3fb_mode, 1);
+	return 0;
+
+err_fb_dealloc:
+	fb_dealloc_cmap(&info->cmap);
+err_framebuffer_release:
+	framebuffer_release(info);
+err_free_irq:
+	 __ps3pf_free_irq(ps3fb.irq_outlet, 
+	 				ps3fb.irq_no, ps3fb.irq_cpu, ps3fb.dev);
+err_gpu_context_free:
+	iounmap((u8 __iomem *)ps3fb.dinfo_ea);
+	lv1_gpu_context_free(ps3fb.context_handle);
+err_gpu_memory_free:
+	iounmap((u8 __iomem *)ps3fb.ddr_ea);
+	lv1_gpu_memory_free(ps3fb.memory_handle);
+	return retval;
+}
+
+static int ps3fb_at_exit(struct notifier_block *self,  unsigned long state,
+						void *data)
+{
+	struct gpu_driver_info *dinfo = (struct gpu_driver_info *)ps3fb.dinfo_ea;
+
+	ps3fb_flip_ctl(0); /* flip off */
+	dinfo->irq.mask = 0; 
+	 __ps3pf_free_irq(ps3fb.irq_outlet, 
+	 				ps3fb.irq_no, ps3fb.irq_cpu, ps3fb.dev);
+	iounmap((u8 __iomem *)ps3fb.dinfo_ea);
+	iounmap((u8 __iomem *)ps3fb.ddr_ea);
+	return NOTIFY_OK;
+}
+
+void ps3fb_cleanup(void)
+{
+	uint64_t status;
+
+	status = lv1_gpu_context_free(ps3fb.context_handle);
+	if (status) {
+		DPRINTK("lv1_gpu_context_free failed: status %ld\n", status);
+	}
+	status = lv1_gpu_memory_free(ps3fb.memory_handle);
+	if (status) {
+		DPRINTK("lv1_gpu_memory_free failed: status %ld\n", status);
+	}
+	ps3av_dev_close();
+}
+EXPORT_SYMBOL(ps3fb_cleanup);
+
+static int ps3fb_remove(struct platform_device *dev)
+{
+	struct fb_info *info = platform_get_drvdata(dev);
+
+	if (info) {
+		unregister_framebuffer(info);
+		fb_dealloc_cmap(&info->cmap);
+		framebuffer_release(info);
+	}
+	ps3fb_cleanup();
+	return 0;
+}
+
+static struct platform_driver ps3fb_driver = {
+	.probe	= ps3fb_probe,
+	.remove = ps3fb_remove,
+	.driver = {
+		.name	= "ps3fb",
+	},
+};
+
+static struct platform_device ps3fb_device = {
+	.name	= "ps3fb",
+	.id	= 0,
+	.dev	= {
+		.release = ps3fb_platform_release,
+	}
+};
+
+static struct notifier_block ps3fb_reboot_nb = {
+	.notifier_call = ps3fb_at_exit
+};
+
+int ps3fb_set_sync(void)
+{
+	uint64_t status;
+
+#ifdef HEAD_A
+	status = lv1_gpu_context_attribute(0x0, 
+					L1GPU_CONTEXT_ATTRIBUTE_DISPLAY_SYNC,
+					0, L1GPU_DISPLAY_SYNC_VSYNC, 0, 0);
+	if (status) {
+		printk("lv1_gpu_context_attribute DISPLAY_SYNC failed: status %ld\n",
+				status);
+		return -1;
+	}
+#endif
+#ifdef HEAD_B
+	status = lv1_gpu_context_attribute(0x0, 
+					L1GPU_CONTEXT_ATTRIBUTE_DISPLAY_SYNC,
+					1, L1GPU_DISPLAY_SYNC_VSYNC, 0, 0);
+
+	if (status) {
+		printk("lv1_gpu_context_attribute DISPLAY_MODE failed: status %ld\n",
+				status);
+		return -1;
+	}
+#endif
+	return 0;
+}
+EXPORT_SYMBOL(ps3fb_set_sync);
+
+static int __init ps3fb_init(void)
+{
+	int ret = 0;
+	uint64_t status;
+#ifndef MODULE
+	int mode;
+	char *option = NULL;
+
+	if (fb_get_options("ps3fb", &option)) {
+		goto err;
+	}
+#endif
+
+	status = ps3av_dev_open();
+	if (status) {
+		printk("ps3av_dev_open failed\n");
+		goto err;
+	}
+
+	/* set gpu-driver internal video mode */
+#ifdef HEAD_A
+	status = lv1_gpu_context_attribute(0x0, 
+					L1GPU_CONTEXT_ATTRIBUTE_DISPLAY_MODE_SET,
+					0, 0, 0, 0);	// head a
+	if (status) {
+		printk("lv1_gpu_context_attribute DISPLAY_MODE failed: status %ld\n",
+				status);
+		goto err;
+	}
+#endif
+#ifdef HEAD_B
+	status = lv1_gpu_context_attribute(0x0, 
+					L1GPU_CONTEXT_ATTRIBUTE_DISPLAY_MODE_SET,
+					0, 0, 1, 0);	// head b
+
+	if (status) {
+		printk("lv1_gpu_context_attribute DISPLAY_MODE failed: status %ld\n",
+				status);
+		goto err;
+	}
+#endif
+
+	ps3fb_mode = ps3av_get_mode();
+	DPRINTK("ps3av_mode:%d\n", ps3fb_mode);
+#ifndef MODULE
+	mode = ps3fb_setup(option); /* check boot option */
+	if (mode)
+		ps3fb_mode = mode;
+#endif
+	if (ps3fb_mode > 0) {
+		uint32_t xres, yres;
+		ps3av_video_mode2res(ps3fb_mode, &xres, &yres);
+		ps3fb.res_index = get_res_table(xres, yres);
+		DPRINTK("res_index:%d\n", ps3fb.res_index);
+	} else {
+		ps3fb.res_index = GPU_RES_INDEX;
+	}
+	ps3fb.videomemorysize = VIDEOMEMSIZE;
+
+	atomic_set(&ps3fb.f_count, -1); /* fbcon opens ps3fb */
+	atomic_set(&ps3fb.ext_flip, 0); /* for flip with vsync */
+	init_MUTEX(&ps3fb.sem);
+	init_waitqueue_head(&ps3fb.wait_vsync);
+	ps3fb.num_frames = PS3FB_NUM_FRAMES_MAX;
+	ret = platform_driver_register(&ps3fb_driver);
+
+	if (!ret) {
+		ret = platform_device_register(&ps3fb_device);
+		if (ret)
+			platform_driver_unregister(&ps3fb_driver);
+	}
+
+	register_reboot_notifier(&ps3fb_reboot_nb);
+	ps3fb_set_sync();
+
+	return ret;
+
+err:
+	return -ENXIO;
+}
+
+module_init(ps3fb_init);
+
+#ifdef MODULE
+static void __exit ps3fb_exit(void)
+{
+	platform_device_unregister(&ps3fb_device);
+	platform_driver_unregister(&ps3fb_driver);
+}
+
+module_exit(ps3fb_exit);
+
+MODULE_LICENSE("GPL");
+#endif				/* MODULE */
Index: linux/drivers/video/Kconfig
===================================================================
--- linux.orig/drivers/video/Kconfig
+++ linux/drivers/video/Kconfig
@@ -1446,6 +1446,14 @@ config FB_VIRTUAL
 	  module will be called vfb.
 
 	  If unsure, say N.
+
+config FB_PS3
+	bool "GPU framebuffer driver"
+	depends on FB && PS3PF_PS3AV
+	select FB_CFB_FILLRECT
+	select FB_CFB_COPYAREA
+	select FB_CFB_IMAGEBLIT
+
 if VT
 	source "drivers/video/console/Kconfig"
 endif
Index: linux/drivers/video/Makefile
===================================================================
--- linux.orig/drivers/video/Makefile
+++ linux/drivers/video/Makefile
@@ -94,6 +94,7 @@ obj-$(CONFIG_FB_TX3912)		  += tx3912fb.o
 obj-$(CONFIG_FB_S1D13XXX)	  += s1d13xxxfb.o
 obj-$(CONFIG_FB_IMX)              += imxfb.o
 obj-$(CONFIG_FB_S3C2410)	  += s3c2410fb.o
+obj-$(CONFIG_FB_PS3)		  += ps3fb.o
 
 # Platform or fallback drivers go here
 obj-$(CONFIG_FB_VESA)             += vesafb.o
Index: linux/include/asm-powerpc/ps3fb.h
===================================================================
--- /dev/null
+++ linux/include/asm-powerpc/ps3fb.h
@@ -0,0 +1,41 @@
+/*
+ * Copyright (C) 2006  Sony Computer Entertainment Inc.
+ *
+ * This program is free software; you can redistribute it and/or modify it
+ * under the terms of the GNU General Public License as published
+ * by the Free Software Foundation; version 2 of the License.
+ *
+ * This program is distributed in the hope that it will be useful, but
+ * WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+ * General Public License for more details.
+ *
+ * You should have received a copy of the GNU General Public License along
+ * with this program; if not, write to the Free Software Foundation, Inc.,
+ * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+ */
+
+#ifndef _PS3FB_H_
+#define _PS3FB_H_
+
+/* ioctl */
+#define PS3FB_IOCTL_SETMODE       _IOW('r',  1, int) /* set video mode */
+#define PS3FB_IOCTL_GETMODE       _IOR('r',  2, int) /* get video mode */
+#define PS3FB_IOCTL_SCREENINFO    _IOR('r',  3, int) /* get screen info */
+#define PS3FB_IOCTL_ON            _IO('r', 4)        /* use IOCTL_FSEL */
+#define PS3FB_IOCTL_OFF           _IO('r', 5)        /* return to normal-flip */
+#define PS3FB_IOCTL_FSEL          _IOW('r', 6, int)  /* blit and flip request */ 
+
+#ifndef FBIO_WAITFORVSYNC
+#define FBIO_WAITFORVSYNC         _IOW('F', 0x20, uint32_t) /* wait for vsync */
+#endif
+
+struct ps3fb_ioctl_res {
+	uint32_t xres; /* frame buffer x_size */
+	uint32_t yres; /* frame buffer y_size */
+	uint32_t xoff; /* margine x  */
+	uint32_t yoff; /* margine y */
+	uint32_t num_frames; /* num of frame buffers */
+};
+
+#endif /* _PS3FB_H_ */
